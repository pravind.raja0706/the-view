import 'package:flutter/material.dart';
import 'package:newTimes/providers/most_popular.dart';
import 'package:provider/provider.dart';

import 'package:newTimes/providers/news.dart';
import 'package:newTimes/screens/homepage.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(
          value: News(),
        ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: HomePage(),
      ),
    );
  }
}

import 'dart:convert';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;

import 'package:newTimes/helpers/servicing_config.dart';
import 'package:newTimes/models/article.dart';
import 'package:newTimes/models/emailed_article.dart';
import 'package:newTimes/models/searchedArticle.dart';
import 'package:newTimes/models/shared_article.dart';
import 'package:newTimes/models/viewed_article.dart';

class News extends ChangeNotifier {
  final String apiKey = "zjE6rhgP32v8NwmukZXs1EuFcQZ8zjfY";
  final String domainUrl = "https://api.nytimes.com/svc";

  List<Article> _searchedNews = [];
  List<Article> _loadingItems = [];
  List<Article> _categoryNews = [];
  List<Article> _loadedItems = [];
  List<ViewedArticle> _loadedViewedItems = [];
  List<SharedArticle> _loadedSharedItems = [];
  List<EmailedArticle> _loadedEmailedItems = [];

  List<EmailedArticle> _popularEmailNews = [];
  List<SharedArticle> _popularSharedNews = [];
  List<ViewedArticle> _popularViewedNews = [];

  List<Article> get searchedNews {
    return [..._searchedNews];
  }

  List<Article> get categoryNews {
    return [..._categoryNews];
  }

  List<EmailedArticle> get popularEmailNews {
    return [..._popularEmailNews];
  }

  List<SharedArticle> get popularSharedNews {
    return [..._popularSharedNews];
  }

  List<ViewedArticle> get popularViewedNews {
    return [..._popularViewedNews];
  }

  String formatter(String date) {
    DateTime parsedDate = DateTime.parse(date);
    var formattedDate = DateFormat('dd/MM/yyyy').format(parsedDate);
    return formattedDate;
  }

  Future<void> getSearchedNews(String category) async {
    final url =
        "$domainUrl/search/v2/articlesearch.json?q=$category&api-key=$apiKey";
    var res = await http.get(url);
    var jsonResponse = json.decode(res.body);
    print(jsonResponse.toString());
    List extractedData = jsonResponse['response']['docs'];
    _loadedItems = [];
    extractedData.forEach((element) {
      _loadingItems.add(Article(
        headline: element['headline']['main'],
        source: element['source'],
        date: formatter(element['pub_date']),
        webUrl: element['web_url'],
      ));
    });
    _searchedNews = _loadingItems;
  }

  Future<void> getCategoriesNews(String category) async {
    final url = "$domainUrl/topstories/v2/$category.json?api-key=$apiKey";
    var res = await http.get(url);
    var jsonResponse = json.decode(res.body);
    List extractedData = jsonResponse['results'];
    _loadedItems = [];
    extractedData.forEach((element) {
      _loadedItems.add(Article(
        headline: element['title'],
        source: element['byline'],
        description: element['abstract'],
        date: formatter(element['published_date']),
        imageUrl: element['multimedia'][0]['url'],
        webUrl: element['url'],
      ));
    });
    _categoryNews = _loadedItems;
  }

  Future<void> getPopularNewsEmailed() async {
    final url = "$MOST_POPULAR_EMAILED";
    var res = await http.get(url);
    print(res.toString());
    var jsonResponse = json.decode(res.body);
    print(jsonResponse.toString());
    List extractedData = jsonResponse['results'];
    _loadedEmailedItems = [];
    extractedData.forEach((element) {
      _loadedEmailedItems.add(EmailedArticle(
        headline: element['title'],
        source: element['byline'],
        description: element['abstract'],
        date: formatter(element['published_date']),
        webUrl: element['url'],
      ));
    });
    _popularEmailNews = _loadedEmailedItems;
  }

  Future<void> getPopularNewsShared() async {
    final url = "$MOST_POPULAR_SHARED";
    var res = await http.get(url);
    var jsonResponse = json.decode(res.body);
    print(jsonResponse.toString());
    List extractedData = jsonResponse['results'];
    _loadedSharedItems = [];
    extractedData.forEach((element) {
      _loadedSharedItems.add(SharedArticle(
        headline: element['title'],
        source: element['byline'],
        description: element['abstract'],
        date: formatter(element['published_date']),
        webUrl: element['url'],
      ));
    });
    _popularSharedNews = _loadedSharedItems;
  }

  Future<void> getPopularNewsViewed() async {
    final url = "$MOST_POPULAR_VIEWED";
    var res = await http.get(url);
    var jsonResponse = json.decode(res.body);
    print(jsonResponse.toString());
    List extractedData = jsonResponse['results'];
    _loadedViewedItems = [];
    extractedData.forEach((element) {
      _loadedViewedItems.add(ViewedArticle(
        headline: element['title'],
        source: element['byline'],
        description: element['abstract'],
        date: formatter(element['published_date']),
        webUrl: element['url'],
      ));
    });
    _popularViewedNews = _loadedViewedItems;
  }
}

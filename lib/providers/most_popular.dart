import 'package:flutter/material.dart';
import 'package:newTimes/models/category.dart';

class MostPopularProv extends ChangeNotifier {
  final List<Category> categoryList = [
    Category(
      name: 'most viewed',
      imageUrl: 'assets/images/most_popular.jpeg',
    ),
    Category(
      name: 'most shared',
      imageUrl: 'assets/images/most_shared.jpeg',
    ),
    Category(
      name: 'most emailed',
      imageUrl: 'assets/images/most_emailed.jpeg',
    ),
  ];
}

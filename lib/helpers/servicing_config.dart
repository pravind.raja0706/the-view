const API_KEY = "zjE6rhgP32v8NwmukZXs1EuFcQZ8zjfY";

const DOMAIN = "https://api.nytimes.com/svc";

const MOST_POPULAR_EMAILED =
    DOMAIN + "/mostpopular/v2/emailed/7.json?api-key=$API_KEY";

const MOST_POPULAR_SHARED =
    DOMAIN + "/mostpopular/v2/shared/1/facebook.json?api-key=$API_KEY";

const MOST_POPULAR_VIEWED =
    DOMAIN + "/mostpopular/v2/viewed/1.json?api-key=$API_KEY";

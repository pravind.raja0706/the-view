class ViewedArticle {
  final String headline;
  final String description;
  final String source;
  final String webUrl;
  final String imageUrl;
  final String date;

  ViewedArticle({
    this.headline,
    this.description,
    this.source,
    this.webUrl,
    this.imageUrl,
    this.date,
  });
}

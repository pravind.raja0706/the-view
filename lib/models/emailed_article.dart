class EmailedArticle {
  final String headline;
  final String description;
  final String source;
  final String webUrl;
  final String imageUrl;
  final String date;

  EmailedArticle({
    this.headline,
    this.description,
    this.source,
    this.webUrl,
    this.imageUrl,
    this.date,
  });
}

import 'package:flutter/material.dart';

class Category extends ChangeNotifier {
  final String name;
  final String imageUrl;

  Category({
    this.name,
    this.imageUrl,
  });
}

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ArticleItem extends StatelessWidget {
  final String headline;
  final String description;
  final String source;
  final String date;

  ArticleItem({
    this.headline,
    this.description,
    this.source,
    this.date,
  });
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        bottom: 8,
      ),
      padding: EdgeInsets.all(5),
      width: double.infinity,
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: 8),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  headline,
                  style: GoogleFonts.bioRhyme(
                      fontSize: 16,
                      letterSpacing: 1,
                      color: Colors.black,
                      fontWeight: FontWeight.w600),
                ),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 5),
                  padding: EdgeInsets.symmetric(horizontal: 2),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        flex: 2,
                        child: Text(
                          source,
                          softWrap: true,
                          style: GoogleFonts.bioRhyme(
                              fontSize: 14,
                              letterSpacing: .75,
                              color: Colors.black,
                              fontWeight: FontWeight.w400),
                        ),
                      ),
                      // Spacer(),
                      Expanded(
                        flex: 1,
                        child: Text(
                          date,
                          softWrap: true,
                          textAlign: TextAlign.right,
                          style: GoogleFonts.bioRhyme(
                              fontSize: 12,
                              letterSpacing: .75,
                              color: Colors.black,
                              fontWeight: FontWeight.w400),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          Divider(
            height: 2,
          ),
        ],
      ),
    );
  }
}

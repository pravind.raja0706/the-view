import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'package:newTimes/screens/searched_article_page.dart';

class Search extends StatefulWidget {
  @override
  _SearchState createState() => _SearchState();
}

class _SearchState extends State<Search> {
  final myController = TextEditingController();
  String _searchWord = "";
  @override
  void dispose() {
    myController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text('THE VIEW',
              style: GoogleFonts.bioRhyme(
                color: Colors.black,
                fontSize: 23,
                fontWeight: FontWeight.w900,
                letterSpacing: 2,
              )),
          iconTheme: IconThemeData(
            color: Colors.black,
          ),
          elevation: 0,
          backgroundColor: Colors.white,
        ),
        body: Container(
          child: Column(
            children: [
              Container(
                width: double.infinity,
                height: 200,
                child: Center(
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Text(
                      'SEARCH',
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: GoogleFonts.coustard(
                        color: Colors.black,
                        fontSize: 22,
                        fontWeight: FontWeight.w700,
                        letterSpacing: 3,
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 20),
                margin: EdgeInsets.only(top: 20, bottom: 10),
                child: TextField(
                  autocorrect: true,
                  style: GoogleFonts.bioRhyme(
                    fontSize: 18,
                  ),
                  decoration: InputDecoration(
                    hintText: 'Search',
                    hintStyle: TextStyle(fontSize: 18),
                    contentPadding:
                        EdgeInsets.symmetric(vertical: 7, horizontal: 18),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(50),
                      ),
                    ),
                  ),
                  controller: myController,
                  onSubmitted: (value) async {
                    setState(
                      () {
                        _searchWord = value;
                        print(_searchWord);
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (ctx) =>
                                SearchedArticleScreen(searchField: _searchWord),
                          ),
                        );
                      },
                    );
                  },
                ),
              ),
              ValueListenableBuilder<TextEditingValue>(
                  valueListenable: myController,
                  builder: (context, value, child) {
                    return GestureDetector(
                      onTap: value.text.isNotEmpty
                          ? () {
                              setState(() {
                                _searchWord = myController.text;
                                print(_searchWord);
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (ctx) => SearchedArticleScreen(
                                        searchField: _searchWord),
                                  ),
                                );
                              });
                            }
                          : null,
                      child: value.text.isNotEmpty
                          ? Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 25, vertical: 12),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(color: Colors.black),
                                  borderRadius: BorderRadius.circular(5)),
                              child: Text(
                                'GO',
                                style: GoogleFonts.bioRhyme(
                                    fontSize: 16,
                                    letterSpacing: .75,
                                    color: Colors.black,
                                    fontWeight: FontWeight.w500),
                              ),
                            )
                          : Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 25, vertical: 12),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(color: Colors.grey),
                                  borderRadius: BorderRadius.circular(5)),
                              child: Text(
                                'GO',
                                style: GoogleFonts.bioRhyme(
                                    fontSize: 16,
                                    letterSpacing: .75,
                                    color: Colors.grey,
                                    fontWeight: FontWeight.w500),
                              ),
                            ),
                    );
                  })
            ],
          ),
        ),
      ),
    );
  }
}

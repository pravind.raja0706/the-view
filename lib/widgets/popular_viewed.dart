import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:google_fonts/google_fonts.dart';

import 'package:newTimes/providers/news.dart';
import 'package:newTimes/widgets/article_list_item.dart';

class PopularViewed extends StatefulWidget {
  @override
  _PopularViewedState createState() => _PopularViewedState();
}

class _PopularViewedState extends State<PopularViewed> {
  bool _isInit = true;
  var _isLoading = false;

  @override
  void didChangeDependencies() {
    if (_isInit) {
      setState(() {
        _isLoading = true;
      });
      Provider.of<News>(context).getPopularNewsViewed().then((_) {
        setState(() {
          _isLoading = false;
        });
      });
    }
    _isInit = false;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return _isLoading
        ? Center(
            child: CircularProgressIndicator(),
          )
        : Column(
            children: [
              Container(
                width: double.infinity,
                height: 100,
                child: Center(
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Text(
                      'MOST VIEWED',
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: GoogleFonts.bioRhyme(
                        color: Colors.black,
                        fontSize: 22,
                        fontWeight: FontWeight.w700,
                        letterSpacing: 3,
                      ),
                    ),
                  ),
                ),
              ),
              Flexible(
                child: Consumer<News>(
                  builder: (ctx, news, child) => ListView.builder(
                    itemCount: news.popularViewedNews.length,
                    itemBuilder: (ctx, index) => ArticleItem(
                      headline: news.popularViewedNews[index].headline,
                      description: news.popularViewedNews[index].description,
                      source: news.popularViewedNews[index].source,
                      date: news.popularViewedNews[index].date,
                    ),
                  ),
                ),
              ),
            ],
          );
  }
}

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'package:newTimes/widgets/popular_emailed.dart';
import 'package:newTimes/widgets/popular_shared.dart';
import 'package:newTimes/widgets/popular_viewed.dart';
import 'package:newTimes/widgets/search.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    super.initState();
  }

  int _selectedIndex = 0;
  static List<Widget> _widgetOptions = <Widget>[
    PopularViewed(),
    PopularShared(),
    PopularEmailed(),
    // Search(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('THE VIEW',
            style: GoogleFonts.bioRhyme(
              color: Colors.black,
              fontSize: 23,
              fontWeight: FontWeight.w900,
              letterSpacing: 2,
            )),
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 5.0),
            child: IconButton(
              icon: Icon(
                Icons.search,
                size: 30,
              ),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (ctx) => Search(),
                  ),
                );
              },
            ),
          ),
        ],
        elevation: 0,
        backgroundColor: Colors.white,
      ),
      body: _widgetOptions.elementAt(_selectedIndex),
      bottomNavigationBar: BottomNavigationBar(
        elevation: 1,
        showSelectedLabels: false,
        showUnselectedLabels: false,
        backgroundColor: Colors.white,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: Icon(
                Icons.remove_red_eye_outlined,
                size: 30,
              ),
              label: "Popular Viewed"),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.screen_share_outlined,
                size: 30,
              ),
              label: "Popular Shared"),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.mark_email_read_outlined,
                size: 30,
              ),
              label: "Popular Emailed"),
          // BottomNavigationBarItem(
          //   icon: Icon(
          //     Icons.search,
          //     size: 30,
          //   ),
          //   label: "Search",
          // ),
        ],
        currentIndex: _selectedIndex,
        unselectedItemColor: Colors.black38,
        selectedItemColor: Colors.black,
        onTap: _onItemTapped,
      ),
    ));
  }
}

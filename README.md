# The View

<hr/>
The View is a simple iOS default UI app that utilises REST APIs from New York Times to perform GET request to display and classify a list of articles based on certain preferred categories. 

This app also showcases the usage of state management called Provider to handle states and receive data from objects declared in models to be used across all pages in the app.

## Getting started

If you would wish to run and debug the source code, make sure to create a New York Times developer account and activate the api keys. Then, replace the keys from this source code to the one you created. 

 ```
I have used these APIs :
> Article Search
> Most Popular (Viewed, Shared and Emailed)
 ```

 - [New York Times API](https://developer.nytimes.com/)



